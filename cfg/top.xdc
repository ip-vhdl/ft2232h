# * Rst / clk

set_property IOSTANDARD LVCMOS33 [get_ports main_rst]
set_property PACKAGE_PIN L20 [get_ports main_rst]

set_property IOSTANDARD LVCMOS33 [get_ports clk]
set_property PACKAGE_PIN F17 [get_ports clk]

# * USB interface

# ** Usb clock

set_property IOSTANDARD LVCMOS33 [get_ports {usb_if[clk_2232]}]
set_property PACKAGE_PIN E18 [get_ports {usb_if[clk_2232]}]
# set frequency to 60 MHz
create_clock -period 16.666 -name clk_usb_time [get_ports {usb_if[clk_2232]}]
# declare async with respecto to rest of clock domains
set_clock_groups -asynch -group [get_clocks clk_usb_time]

# ** usb_data

set_property IOSTANDARD LVTTL [get_ports {usb_if[usb_data][7]}]
set_property PACKAGE_PIN B16 [get_ports {usb_if[usb_data][7]}]

set_property IOSTANDARD LVTTL [get_ports {usb_if[usb_data][6]}]
set_property PACKAGE_PIN A18 [get_ports {usb_if[usb_data][6]}]

set_property IOSTANDARD LVTTL [get_ports {usb_if[usb_data][5]}]
set_property PACKAGE_PIN A19 [get_ports {usb_if[usb_data][5]}]

set_property IOSTANDARD LVTTL [get_ports {usb_if[usb_data][4]}]
set_property PACKAGE_PIN B17 [get_ports {usb_if[usb_data][4]}]

set_property IOSTANDARD LVTTL [get_ports {usb_if[usb_data][3]}]
set_property PACKAGE_PIN A17 [get_ports {usb_if[usb_data][3]}]

set_property IOSTANDARD LVTTL [get_ports {usb_if[usb_data][2]}]
set_property PACKAGE_PIN C19 [get_ports {usb_if[usb_data][2]}]

set_property IOSTANDARD LVTTL [get_ports {usb_if[usb_data][1]}]
set_property PACKAGE_PIN B19 [get_ports {usb_if[usb_data][1]}]

set_property IOSTANDARD LVTTL [get_ports {usb_if[usb_data][0]}]
set_property PACKAGE_PIN C17 [get_ports {usb_if[usb_data][0]}]

# ** Ctrl

# usb_rxf

set_property IOSTANDARD LVCMOS33 [get_ports {usb_if[usb_rxf]}]
set_property PACKAGE_PIN E15 [get_ports {usb_if[usb_rxf]}]
set_property PULLUP true [get_ports {usb_if[usb_rxf]}]

# usb_txe

set_property IOSTANDARD LVCMOS33 [get_ports {usb_if[usb_txe]}]
set_property PACKAGE_PIN E16 [get_ports {usb_if[usb_txe]}]
set_property PULLUP true [get_ports {usb_if[usb_txe]}]

# usb_rd

set_property IOSTANDARD LVCMOS33 [get_ports {usb_if[usb_rd]}]
set_property PACKAGE_PIN G17 [get_ports {usb_if[usb_rd]}]

# usb_wr

set_property IOSTANDARD LVCMOS33 [get_ports {usb_if[usb_wr]}]
set_property PACKAGE_PIN F18 [get_ports {usb_if[usb_wr]}]

# usb_oe

set_property IOSTANDARD LVCMOS33 [get_ports {usb_if[usb_oe]}]
set_property PACKAGE_PIN C16 [get_ports {usb_if[usb_oe]}]
