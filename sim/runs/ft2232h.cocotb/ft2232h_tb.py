#!/usr/bin/python

"""
Documentation.
"""

# TODO: replace random by https://github.com/mciepluc/cocotb-coverage

# * Imports

import os
import cocotb
from cocotb.triggers import Timer, FallingEdge, with_timeout, First, Lock
from cocotb.clock import Clock
from cocotb.binary import BinaryValue
from cocotb.utils import get_sim_time
from cocotb import fork
from edacommonlib.models.buses.usb.bus_usb import usb, WriteTransactionException, ReadTransactionException
from cocotb.result import SimTimeoutError, SimFailure, TestError, TestSuccess
from edacommonlib.testbench.testbench import tb_base


# * Testbench class


class tb_ft2232h(tb_base):
    """
    Testbench class. Implements infrastructure for testing dut.
    """

    # ** Init

    def __init__(self, dut):
        """
        Initialize things.
        """

        tb_base.__init__(self, dut)

        # create instance of usb class
        self.usb = usb(dut, "usb", dut.clk_2232)

        # ii. get generics as env variables
        self.TIMEOUT = int(os.environ['TIMEOUT'])
        self.C_REGBANKLENGTH = int(os.environ['C_REGBANKLENGTH'])
        self.TEST_READBACK_NB_TIMES = int(os.environ['TEST_READBACK_NB_TIMES'])

        # initial reset
        # self.reset_task = fork(self.tb_base_reset())

    # ** Write commands

    async def command_p1(self, params):
        """
        Send command 01 with PARAMS.
        """

        self.tb_base_message("command p1 (0x01): sending command", 'debug')

        try:
            # use a transfert in one single burst
            await self.usb.write(0x01, params)
        except WriteTransactionException as e:
            self.tb_base_message("command 01: {:s}".format(e), 'error')
            raise TestError

        self.tb_base_message("command p1 (0x01): done command", 'debug')

    async def command_03(self, params):
        """
        Send command 03 with PARAMS.
        """

        self.tb_base_message("command 03: sending command", 'debug')

        try:
            # use a transfert in one single burst
            await self.usb.write(0x03, params)
        except WriteTransactionException as e:
            self.tb_base_message("command 03: {:s}".format(e), 'error')
            raise TestError

        self.tb_base_message("command 03: done command", 'debug')

    async def command_readback(self, params):
        """
        Sends PARAMS, corresponds to order 05.
        PARAMS are returned.
        """

        self.tb_base_message("command readback (0x05): sending command", 'debug')

        try:
            # use a transfert in two bursts
            await self.usb.write_2(0x05, params)
        except WriteTransactionException as e:
            self.tb_base_message("command 05: {:s}".format(e), 'error')
            raise TestError

        self.tb_base_message("command readback (0x05): done command", 'debug')

# ** Tests

    # *** Readback

    async def test_readback(self, nb_bytes):
        """
        Readback test.
        Sends a random array of NB_BYTES and reads it back again. Then, compares
        the two arrays.
        Raises a TestError exception in case of failure.
        """

        import random

        self.tb_base_message("test_readback: running test ...", 'info')

        if nb_bytes > self.C_REGBANKLENGTH:
            raise ValueError
        # reference list to compare results with
        reference = [random.randrange(0, 255, 1) for i in range(nb_bytes)]
        reference.insert(0, 5)
        reference.append(255)
        # send command, blocks until complete
        # await self.command_readback(reference[1:-1])
        # non blocking
        fork(self.command_readback(reference[1:-1]))
        # Read data. See [[# ** Blocking write / read.][Blocking write/read]] note below.
        data = await self.usb.read(len(reference))
        if data != reference:
            raise TestError
        else:
            self.tb_base_message("test_readback: done test.", 'info')

    # *** Peripheral

    async def test_peripheral_p1(self, nb_bytes):
        """
        Simple peripheral test.
        Test of NB_BYTES (<65535, multiple of 4) data exchange with p1,
        corresponding to order 0x01.
        Raises a TestError exception in case of failure.
        """

        self.tb_base_message("p1_test: running p1 test ...", 'info')

        if (nb_bytes % 4 != 0) & (nb_bytes < 65536):
            raise ValueError
        # prepare params
        a, b = divmod(nb_bytes, 64)
        # send command, blocks until complete
        # await self.command_p1([0, 0, a, b])
        # non blocking
        fork(self.command_p1([0, 0, a, b]))
        # Read data. See [[# ** Blocking write / read.][Blocking write/read]] note below.
        data = await self.usb.read(nb_bytes)
        # reference list to compare results with
        reference = list(map(lambda x: x % 256, list(range(nb_bytes))))
        if data != reference:
            raise TestError
        else:
            self.tb_base_message("p1_test: done test.", 'info')


# * Test command 3

@cocotb.test()
async def test_command3(dut):
    """
    Tests command 3.
    Just calls command 3 once. Useful when setting DEBUG=1 to inspect traces.
    """

    tb = tb_ft2232h(dut)
    tb.tb_base_message("Starting running command 3 test", 'info')

    # tb.dut._log.setLevel(logging.DEBUG)

    # Wait to start testing until reset task is over
    # tb.tb_base_message(await Join(tb.reset_task), 'debug')
    tb.tb_base_message("Waiting for init reset ...", 'debug')
    await FallingEdge(dut.rst)
    tb.tb_base_message("... initial reset is over", 'debug')

    ####################
    #  Test 1          #
    #  Send command 03 #
    ####################

    task = fork(tb.command_03(list(range(34, 40))))

    try:
        await with_timeout(task, tb.TIMEOUT, 'us')
    except SimTimeoutError as e:
        dut._log.error("exception SimTimeoutError: %s" % (e))
        raise TestError

    tb.tb_base_message("End running command 3 test", 'info')

    raise TestSuccess


# * Test readback

@cocotb.test()
async def test_readback(dut):
    """
    Test readback.
    Performs readback test using a C_REGBANKLENGTH array, looping TEST_READBACK_NB_TIMES times.
    """

    tb = tb_ft2232h(dut)  # create tb instance

    tb.tb_base_message("Start running readback test", 'info')

    # Wait to start testing until reset task is over
    # tb.tb_base_message(await Join(tb.reset_task), 'debug')
    tb.tb_base_message("Waiting for init reset ...", 'debug')
    await FallingEdge(dut.rst)
    tb.tb_base_message("... initial reset is over", 'debug')

    for i in range(tb.TEST_READBACK_NB_TIMES):

        tb.tb_base_message("\tIteration {}".format(i), 'info')

        task = fork(tb.test_readback(tb.C_REGBANKLENGTH-2))  # fork test

        try:
            await with_timeout(task, tb.TIMEOUT, 'us')
        except SimTimeoutError as e:
            dut._log.error("exception SimTimeoutError: %s" % (e))
            raise TestError
        except ValueError:
            dut._log.error("test_readback: incorrect nb_bytes value. Check docstring.")
            raise TestError
        except TestError:
            dut._log.error("test_readback: test error.")
            return

    # Display coverage reporting information
    tb.tb_base_message("Monitor at", 'info')
    dut.osvvm_do_report <= 1

    # Let some delta cycles so that the report may be issued by the simulator
    await Timer(1, units='ns')

    tb.tb_base_message("End running readback test", 'info')

    raise TestSuccess


# * Test peripheral

@cocotb.test()
async def test_peripheral_p1(dut):
    """
    Tests peripheral.
    Just calls peripheral test once.
    """

    tb = tb_ft2232h(dut)         # create tb instance

    tb.tb_base_message("Start running peripheral test", 'info')

    # Wait to start testing until reset task is over
    # tb.tb_base_message(await Join(tb.reset_task), 'debug')
    tb.tb_base_message("Waiting for init reset ...", 'debug')
    await FallingEdge(dut.rst)
    tb.tb_base_message("... initial reset is over", 'debug')

    task = fork(tb.test_peripheral_p1(16*4*100))  # fork test

    try:
        await with_timeout(task, tb.TIMEOUT, 'us')
    except SimTimeoutError as e:
        dut._log.error("exception SimTimeoutError: %s" % (e))
        raise TestError
    except ValueError:
        dut._log.error("test_p1: incorrect nb_bytes value. Check docstring.")
        raise TestError
    except TestError:
        dut._log.error("test_p1: test error.")
        return

    # Display coverage reporting information
    tb.tb_base_message("Monitor at", 'info')
    dut.osvvm_do_report <= 1

    # Let some delta cycles so that the report may be issued by the simulator
    await Timer(1, units='ns')

    tb.tb_base_message("End running peripheral test", 'info')

    raise TestSuccess


# * Test dummy

@cocotb.test()
async def test_dummy(dut):
    """
    Tests dummy.
    Dummy test whose only purpose is force building the simulation executable.
    """

    tb = tb_ft2232h(dut)

    tb.tb_base_message("Start running dummy test", 'info')

    # Wait to start testing until reset task is over
    # tb.tb_base_message(await Join(tb.reset_task), 'debug')
    tb.tb_base_message("Waiting for init reset ...", 'debug')
    await FallingEdge(dut.rst)
    tb.tb_base_message("... initial reset is over", 'debug')

    tb.tb_base_message("End running dummy test", 'info')

    raise TestSuccess


# * Notes
#
# ** Blocking write / read.
#
# In case of non blocking write (fork), read starts at the same time as write (check log times
# setting logging to debug). In this case, read waits until data is available.
#
# With a blocking write, read starts sequentially once the write is over.
#
# Note that here, the vhdl implementation handles this nicelly: even if a read request happens when
# no data is available, the request stands by for incoming data. Once data reaches output point, the
# read request starts. On top of that, the usb bus python implemnetation includes a lock to avoid
# conflicts of concurrent accessing of the bus: this is the general way of doing things, even if in
# this particular case this would’t be necessary.
