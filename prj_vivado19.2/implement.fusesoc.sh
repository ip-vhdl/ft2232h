#!/bin/sh

#!/bin/sh

# setup fusesoc
export fusesoc_tmp=/tmp/fusesoc
[ -e $fusesoc_tmp ] && rm -rf $fusesoc_tmp
mkdir -p $fusesoc_tmp/fusesoc_libraries
touch $fusesoc_tmp/fusesoc.conf
alias fs='fusesoc --config $fusesoc_tmp/fusesoc.conf'

# add ip_lib library
fs library add --location $fusesoc_tmp/fusesoc_libraries/ip_lib --sync-type git ip_lib https://gitlab.com/ip-vhdl/ip_lib
fs library update
fs library list

# check ip core
fs core list
fs core show ft2232h

# implement
cd /tmp && fs run --target=implement --tool=vivado ft2232h
