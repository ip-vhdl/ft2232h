library ieee;
use ieee.std_logic_1164.all;

package top_pkg is

  constant RegBankLength : natural := 560;

end top_pkg;

package ft2232h_instance_pkg is new work.ft2232h_pkg
    generic map (p1_data_width   => 16,
                 p1_params_width => 4,
                 --
                 p2_data_width   => 8,
                 p2_params_width => 4,
                 --
                 p3_data_width   => 1,
                 p3_params_width => 66,
                 --
                 p4_data_width   => 16,
                 p4_params_width => 2,
                 --
                 p5_data_width   => 1,
                 p5_params_width => 3,
                 --
                 p6_data_width   => 16,
                 p6_params_width => 4,
                 --
                 RegBankLength   => work.ft2232h_tb_pkg.RegBankLength,
                 nb_peripherals  => 6);
