library ieee;
use ieee.std_logic_1164.all;
use work.ft2232h_instance_pkg.all;

entity top is
  generic (nb_asics : natural range 1 to 8 := 8);
  port (main_rst : in    std_logic;
        clk      : in    std_logic;
        -- ftdi usb if
        usb_if   : inout rc_usb_if);
end entity top;

architecture simple of top is

  signal p1_if : rc_peripheral_if_p1(params(0 to 0));
  signal p2_if : rc_peripheral_if_p2(params(0 to nb_asics-1));
  signal p3_if : rc_peripheral_if_p3(params(0 to nb_asics-1));
  signal p4_if : rc_peripheral_if_p4(params(0 to 0));
  signal p5_if : rc_peripheral_if_p5(params(0 to 0));
  signal p6_if : rc_peripheral_if_p6(params(0 to 0));

  signal global_reset : std_logic := '0';

begin

  u_ft2232h : entity work.ft2232h
    port map (main_rst      => main_rst,
              usb_if        => usb_if,
              p1_if         => p1_if,
              p2_if         => p2_if,
              p3_if         => p3_if,
              p4_if         => p4_if,
              p5_if         => p5_if,
              p6_if         => p6_if,
              disable_40MHz => open,
              global_reset  => global_reset);

  u_p1_counter : entity work.p1_counter
    port map (clk          => clk,
              global_reset => global_reset,
              p_if         => p1_if);

end architecture simple;
