library IEEE;
use IEEE.STD_LOGIC_1164.all;

library osvvm;
context osvvm.OsvvmContext;

package ft2232h_pkg is

  generic (RegBankLength : natural := 70 * 8;

           nb_peripherals : natural range 1 to 8 := 6;

           p1_data_width   : natural range 1 to 16 := 16;  -- bytes
           p1_params_width : natural range 1 to 4  := 4;   -- bytes

           p2_data_width   : natural range 1 to 16 := 8;  -- bytes
           p2_params_width : natural range 1 to 4  := 4;  -- bytes

           p3_data_width   : natural range 1 to 8  := 1;   -- bytes
           p3_params_width : natural range 1 to 66 := 66;  -- bytes

           p4_data_width   : natural range 1 to 16 := 16;  -- bytes
           p4_params_width : natural range 1 to 8  := 2;   -- bytes

           p5_data_width   : natural range 1 to 16 := 1;  -- bytes
           p5_params_width : natural range 1 to 8  := 3;  -- bytes

           p6_data_width   : natural range 1 to 16 := 16;  -- bytes
           p6_params_width : natural range 1 to 8  := 4    -- bytes
           );

  -- ** Types

  -- Register bank to store slow control orders from usb.

  type regbank is array(0 to RegBankLength) of std_logic_vector(7 downto 0);

  -- *** Peripherals

  type rc_params_p1 is array (natural range <>) of std_logic_vector(p1_params_width * 8 - 1 downto 0);

  type rc_params_p2 is array (natural range <>) of std_logic_vector(p2_params_width * 8 - 1 downto 0);

  type rc_params_p3 is array (natural range <>) of std_logic_vector(p3_params_width * 8 - 1 downto 0);

  type rc_params_p4 is array (natural range <>) of std_logic_vector(p4_params_width * 8 - 1 downto 0);

  type rc_params_p5 is array (natural range <>) of std_logic_vector(p5_params_width * 8 - 1 downto 0);

  type rc_params_p6 is array (natural range <>) of std_logic_vector(p6_params_width * 8 - 1 downto 0);

  -- subtype rc_params_p1 is array (0 to 0) of std_logic_vector(p1_params_width*8-1 downto 0);

  type rc_peripheral_if_p1 is record
    clk    : std_logic;
    -- in
    params : rc_params_p1;
    enable : std_logic;
    -- out
    rd     : std_logic;
    data   : std_logic_vector(p1_data_width * 8 - 1 downto 0);
    empty  : std_logic;
  end record rc_peripheral_if_p1;

  type rc_peripheral_if_p2 is record
    clk    : std_logic;
    -- in
    params : rc_params_p2;
    enable : std_logic;
    -- out
    rd     : std_logic;
    data   : std_logic_vector((p2_data_width + 1) * 8 - 1 downto 0);
    empty  : std_logic;
  end record rc_peripheral_if_p2;

  type rc_peripheral_if_p3 is record
    clk    : std_logic;
    -- in
    params : rc_params_p3;
    enable : std_logic;
    -- out
    rd     : std_logic;
    data   : std_logic_vector((p3_data_width + 1) * 8 - 1 downto 0);
    empty  : std_logic;
  end record rc_peripheral_if_p3;

  type rc_peripheral_if_p4 is record
    clk    : std_logic;
    -- in
    params : rc_params_p4;
    enable : std_logic;
    -- out
    rd     : std_logic;
    data   : std_logic_vector(p4_data_width * 8 - 1 downto 0);
    empty  : std_logic;
  end record rc_peripheral_if_p4;

  type rc_peripheral_if_p5 is record
    clk    : std_logic;
    -- in
    params : rc_params_p5;
    enable : std_logic;
    -- out
    rd     : std_logic;
    data   : std_logic_vector(p5_data_width * 8 - 1 downto 0);
    empty  : std_logic;
  end record rc_peripheral_if_p5;

  type rc_peripheral_if_p6 is record
    clk    : std_logic;
    -- in
    params : rc_params_p6;
    enable : std_logic;
    -- out
    rd     : std_logic;
    data   : std_logic_vector(p6_data_width * 8 - 1 downto 0);
    empty  : std_logic;
  end record rc_peripheral_if_p6;

  -- *** Usb if

  type rc_usb_if is record
    usb_rxf   : std_logic;
    usb_siwua : std_logic;
    usb_txe   : std_logic;
    clk_2232  : std_logic;                     -- usb clock, 60 MHz
    usb_oe    : std_logic;
    usb_rd    : std_logic;
    usb_wr    : std_logic;
    usb_data  : std_logic_vector(7 downto 0);  -- usb IO data bus
  end record rc_usb_if;

  -- *** Test
  --
  -- Test code, the day ghdl accepts it completely, it may be useful to simplify
  -- peripheral type handling

  type rc_params is array (natural range <>) of std_logic_vector(7 downto 0);

  type rc_params_array is array (natural range <>) of rc_params;

  type rc_peripheral_if is record
    clk    : std_logic;
    -- in
    params : rc_params_array;
    enable : std_logic;
    -- out
    rd     : std_logic;
    data   : std_logic_vector(8 - 1 downto 0);
    empty  : std_logic;
  end record rc_peripheral_if;

  -- This is accepted by ghdl
  signal titi : rc_params_array(0 to 4)(0 to 4);

  -- This is not accepted
  -- signal titi : rc_peripheral_if(params(0 to 4)(0 to 4));

  function order_to_string (
    signal order : std_logic_vector)
    return string;

  procedure ft2232h_log_out_order (signal order : in std_logic_vector);

  procedure ft2232h_log_in_order (signal data    : in std_logic_vector;
                                  signal address : in natural);

end package ft2232h_pkg;

package body ft2232h_pkg is

  function order_to_string (
    signal order : std_logic_vector)
    return string is
  begin

    case order is

      when X"05" =>
        return "readback";
      when X"CC" =>
        return "catiroc slow control";
      when X"01" =>
        return "p1 peripheral";
      when others =>
        return "unknown order";

    end case;

  end function order_to_string;

  procedure ft2232h_log_out_order (
    signal order : in std_logic_vector) is
  begin
    Log("usb out: order " & order_to_string(order) & " - " &
        to_hstring(order) & " - " & " requested ", DEBUG);
  end procedure ft2232h_log_out_order;

  procedure ft2232h_log_in_order (
    signal data    : in std_logic_vector;
    signal address : in natural) is
  begin
    Log("usb in: data captured, " & to_string(address) & " - data " & to_hstring(data), DEBUG);
  end procedure ft2232h_log_in_order;

end package body ft2232h_pkg;
